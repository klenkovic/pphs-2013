"""
 * promjenit kod da radi sa matricama veličine 20 * 20 i vektorima veličine 20
 * matrica 30 * 20
"""
import pycuda.autoinit
import pycuda.driver as drv
import pycuda.gpuarray as ga
import numpy as np
from pycuda.compiler import SourceModule

mod = SourceModule(open("1.cu").read())

mat_vek = mod.get_function("mat_vek")

a = np.ones((30, 20), dtype=np.float32) 
b = np.ones((20), dtype=np.float32)

result_gpu = np.empty(30, dtype=np.float32)

mat_vek(drv.Out(result_gpu), drv.In(a), drv.In(b), block=(30,1,1), grid=(1,1))

result_cpu = np.dot(a, b)

print("CPU: ", result_cpu)
print("GPU: ", result_gpu)
