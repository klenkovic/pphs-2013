"""
 * Prilagodite kod tako da množi matrice formata (200, 200) u 10 blokova po x 
   kordnati i 10 blokova po y kordinati. Uputa za indeksiranje:
   * blockIdx.y * gridDim.x * blockDim.y * blockDim.x,
   * threadIdx.y * blockDim.x,
   * blockIdx.x * blockDim.y * blockDim.x,
   * threadIdx.x.
"""
import pycuda.autoinit
import pycuda.driver as drv
import pycuda.gpuarray as ga
import numpy as np
from pycuda.compiler import SourceModule

mod = SourceModule(open("3.cu").read())

mat_vek = mod.get_function("matrix_mult")

a = np.ones((200, 200), dtype=np.float32) 
b = np.ones((200, 200), dtype=np.float32)

result_gpu = np.empty((200, 200), dtype=np.float32)

mat_vek(drv.Out(result_gpu), drv.In(a), drv.In(b), block=(20,20,1), grid=(10,10))
result_cpu = np.dot(a, b)

print("CPU: ", result_cpu)
print("GPU: ", result_gpu)
