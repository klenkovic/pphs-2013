__global__ void mat_vek (float *dest, float *a, float *b)
{
  // NE RADI
  __shared__ float cache[8];

  const int i1 = threadIdx.x;
  const int i2 = blockIdx.x;
  const int i3 = blockIdx.x * blockDim.x + threadIdx.x;
  cache[i1] = a[i3] * b[i1];
  int active = blockDim.x;
  if (blockDim.x % 2 != 0)
    {
      active++;
    }
  active /= 2;

  do
    {
      __syncthreads();
      if (i1 < active)
        {
	  cache[i1] = cache[i1] + cache[i1 + active];
        }
      active /= 2;
    }
  while (active > 0);

  if (i1 == 0)
    {
      dest[i2] = cache[0];
    }
}
