__global__ void matrix_mult (float *result, float *mat1, float *mat2)
{
  int tx = threadIdx.x;
  int ty = threadIdx.y;

  float value = 0;
  for (int k = 0; k < 200; k++)
    {
      value += mat1[ty * blockDim.x + k] * mat2[k * blockDim.x + tx]; // dovrsit
    }

  result[blockIdx.y * gridDim.x * blockDim.y * blockDim.x + threadIdx.y * blockDim.x + blockIdx.x * blockDim.y * blockDim.x + threadIdx.x] = value;
}
