__global__ void mat_vek (float *dest, float *a, float *b)
{
  const int i = threadIdx.x; 
  dest[i] = 0;
  for (int x = 0; x < 20; x++)
    {
      dest[i] += a[x] * b[x];
    }
}
