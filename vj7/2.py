"""
 * što se desi kad imamo matrice ne-2^n-tu formata, tipa 25*25
"""
import pycuda.autoinit
import pycuda.driver as drv
import pycuda.gpuarray as ga
import numpy as np
from pycuda.compiler import SourceModule

mod = SourceModule(open("2.cu").read())

mat_vek = mod.get_function("mat_vek")

a = np.ones((25, 25), dtype=np.float32) 
b = np.ones((25), dtype=np.float32)

result_gpu = np.empty(8, dtype=np.float32)

mat_vek(drv.Out(result_gpu), drv.In(a), drv.In(b), block=(25,1,1), grid=(25,1))
result_cpu = np.dot(a, b)

print("CPU: ", result_cpu)
print("GPU: ", result_gpu)
