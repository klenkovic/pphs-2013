__global__ void vector_sum (float *dest, float *a, float *b, float *c)
{
  const int i = blockIdx.x * blockDim.x + threadIdx.x;
  //             525           0 | 1        0..524

  dest[i] = a[i] + b[i] + c[i];
}
