"""
 * Promijenite kod primjera tako da umjesto traženja minimuma traži maksimum 
   i dodajte printf na odgovarajuće mjesto kako bi vidjeli vrijednosti varijabli 
   idx, active i elemenata u polju cache s kojima ta nit radi.
 * Promijenite kod primjera tako da umjesto traženja minimuma, odnosno maksimuma, 
   vrši zbrajanje.
"""
import pycuda.autoinit
import pycuda.driver as drv
import numpy as np
from pycuda.compiler import SourceModule

mod = SourceModule(open("3.cu").read())

find_max = mod.get_function("find_max")

a = np.random.rand(16).astype(np.float32)
result_gpu = np.empty(1).astype(np.float32)

find_max(drv.Out(result_gpu), drv.In(a), block=(16,1,1), grid=(1,1))

result_cpu = max(a)
print("početno polje\n", a)
print("max", result_gpu)
print("max (prema CPU-u)", result_cpu)
