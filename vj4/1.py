import pycuda.autoinit
import pycuda.driver as drv
import numpy as np
from pycuda.compiler import SourceModule

mod = SourceModule(open("1.cu").read())

zbroj_vektora = mod.get_function("vector_sum")

a = np.ones(1050, dtype=np.float32)
b = np.ones(1050, dtype=np.float32)
c = np.ones(1050, dtype=np.float32)

result_gpu = np.zeros_like(a)

zbroj_vektora(drv.InOut(result_gpu), drv.In(a), drv.In(b), drv.In(c), block=(525,1,1), grid=(2,1))

result_cpu = a + b + c

np.set_printoptions(threshold=np.nan)

print("CPU rezultat\n", result_cpu)
print("GPU rezultat\n", result_gpu)
#print("CPU i GPU daju isti rezultat?\n", result_cpu == result_gpu)
