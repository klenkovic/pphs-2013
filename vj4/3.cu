__global__ void find_max (float *dest, float *src)
{
  __shared__ float cache[16];

  const int idx = threadIdx.x;
  cache[idx] = src[idx];
  int active = blockDim.x / 2;

  do
    {
      __syncthreads();
      if (idx < active) 
        {
	  // printf ("idx = %f\n active = %f\n cache[idx] = %f\n", idx, active, cache[idx]); TODO
          cache[idx] = fmax(cache[idx], cache[idx + active]); // cache[idx] = fmax(cache[idx], cache[idx + active]);
        }
      active /= 2;
    }
  while (active > 0);

  if (idx == 0)
    {
      dest[blockIdx.x] = cache[0];
    }
}
