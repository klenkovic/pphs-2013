__global__ void zbroj_vektora (float *dest, float *a, float *b, float *c)
{
  //               525         0 ili 1       0, 1, ... , 524
  const int i = blockDim.x * blockIdx.x + threadIdx.x;
  dest[i] = 2 * a[i] + b[i] + c[i]; // dest[i] = 2*a[i] + b[i];
}
