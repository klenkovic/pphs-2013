"""
 * ispisati 524. i 525. element 
"""

import pycuda.autoinit
import pycuda.driver as drv
import numpy as np
from pycuda.compiler import SourceModule

mod = SourceModule(open("primjer_vektor.cu").read())

zbroj_vektora = mod.get_function("zbroj_vektora")

a = np.ones(1050, dtype=np.float32)
b = np.ones(1050, dtype=np.float32)
c = np.ones(1050, dtype=np.float32)

result_gpu = np.zeros_like(a)

zbroj_vektora (drv.InOut(result_gpu), drv.In(a), drv.In(b), drv.In(c), 
               block=(525,1,1), grid=(2,1)) 

result_cpu = 2 * a + b + c

print(result_gpu)
print(result_gpu[524], result_gpu[525])
#print(result_cpu)
