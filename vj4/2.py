"""
 * Provjerite možete li se s dva bloka izvesti zbrajanje vektora od 3000 
   elemenata. (Čim je zadano ovako, vjerojatno ne možete.)
 * Izvedite zbrajanje vektora od 3000 elemenata u 3 bloka. 
"""

import pycuda.autoinit
import pycuda.driver as drv
import numpy as np
from pycuda.compiler import SourceModule

mod = SourceModule(open("primjer_vektor.cu").read())

zbroj_vektora = mod.get_function("zbroj_vektora")

a = np.ones(3000, dtype=np.float32)
b = np.ones(3000, dtype=np.float32)
c = np.ones(3000, dtype=np.float32)

result_gpu = np.zeros_like(a)

zbroj_vektora (drv.InOut(result_gpu), drv.In(a), drv.In(b), drv.In(c), 
               block=(1000,1,1), grid=(3,1)) # limit za block 1024 

print(result_gpu)
