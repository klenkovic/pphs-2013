#include <stdio.h>

__global__ void f_sum (float *dest, float *src)
{
  __shared__ float cache[16];

  const int idx = threadIdx.x;
  cache[idx] = src[idx];
  int active = blockDim.x / 2;

  do
    {
      __syncthreads();
      if (idx < active)
        {
          cache[idx] += cache[idx + active];
        }
      active /= 2;
    }
  while (active > 0);

  if (idx == 0)
    {
      dest[blockIdx.x] = cache[0];
    }
}
