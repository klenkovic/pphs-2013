"""
 * Promijenite kod primjera tako da umjesto traženja 
 minimuma traži maksimum i dodajte printf na odgovarajuće 
 mjesto kako bi vidjeli vrijednosti varijabli idx, active 
 i elemenata u polju cache s kojima ta nit radi.
"""
import pycuda.autoinit
import pycuda.driver as drv
import numpy as np
from pycuda.compiler import SourceModule

mod = SourceModule(open("1.cu").read())

find_min = mod.get_function("find_min")

a = np.random.rand(16).astype(np.float32)
result_gpu = np.empty(1).astype(np.float32)

find_min(drv.Out(result_gpu), drv.In(a), block=(16,1,1), grid=(1,1))

result_cpu = max(a)

print(a)
print("GPU:", result_gpu)
print("CPU:", result_cpu)
