"""
 * Promijenite kod primjera tako da umjesto traženja 
 minimuma, odnosno maksimuma, vrši zbrajanje. 
"""
import pycuda.autoinit
import pycuda.driver as drv
import numpy as np
from pycuda.compiler import SourceModule

mod = SourceModule(open("2.cu").read())

f_sum = mod.get_function("f_sum")

a = np.ones(16).astype(np.float32)
result_gpu = np.empty(1).astype(np.float32)

f_sum(drv.Out(result_gpu), drv.In(a), block=(16,1,1), grid=(1,1))

result_cpu = sum(a)

print(a)
print("GPU:", result_gpu)
print("CPU:", result_cpu)
