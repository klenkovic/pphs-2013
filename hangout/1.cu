// C kod
#include <math.h>

__device__ float zbrajanje(float a, float b){
  return 2 * sin(a) + 3 * cos(b) + 4;
}

__global__ void upis_ispis(float *a, float *b, float *c){

  const int i = threadIdx.y * blockDim.x + threadIdx.x;

  c[i] = zbrajanje(a[i],b[i]);

}
