#////////////////////////////////////////////////////////////////////////////
 
import pycuda.autoinit
import pycuda.driver as drv
import pycuda.gpuarray as ga
import numpy as np
from pycuda.compiler import SourceModule
import pycuda.curandom
 
mod = SourceModule('''

__device__ int get_idx(void){return blockDim.x*blockDim.y*blockIdx.x + threadIdx.x;}
 
__global__ void temp(float *temperature, float *values, int *koliko){
  __shared__ float cache[16];
  const int idx = get_idx();
  const int tidx = threadIdx.x;
  cache[tidx] = values[idx];
  int active = blockDim.x / 2;
 
  do
    {
      __syncthreads();
      if (tidx < active)
        {
          // redukcija korištenjem zbrajanja                                                                                                  
          cache[tidx] += cache[tidx + active];
        }
      active /= 2;
    }
  while (active > 0);
 
  if (tidx == 0)
    {
      temperature[blockIdx.x] = cache[0]/koliko[blockIdx.x];
    }
 
}

''')
temp = mod.get_function("temp")
 
vrijednosti = np.random.uniform(-5, 35, [4,16])
vrijednosti = vrijednosti.astype(np.float32)
 
temperature = np.ones(4) * -1
temperature = temperature.astype(np.float32)
 
koliko = np.zeros(4)
koliko = koliko.astype(np.int32)
 
for i in range(4):
    c=0
    for j in range(16):
        if vrijednosti[i][j]!=0 :
            c+=1
    koliko[i]=c
 
print(koliko)
 
temp(drv.Out(temperature), drv.In(vrijednosti), drv.In(koliko), block=(16,1,1), grid=(4,1))
 
print(vrijednosti)
print(temperature)
