# -*- coding: utf-8 -*-                                                                   
import pycuda.autoinit
import pycuda.gpuarray as ga
import numpy as np

vektori = ga.to_gpu(np.ones((10,5),dtype=np.float32)+2)
# ili ovako:
#vektori = ga.zeros((10, 5), dtype=np.float32)+3
zbroj = ga.zeros((45, 5), dtype=np.float32)

print(vektori)

#k = 0
for i in range(10):
    for j in range (i + 1, 10):
        zbroj = vektori[i] + vektori[j]
        print(zbroj)
        #k += 1
