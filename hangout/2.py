# -*- coding: utf-8 -*-                                                                   
import pycuda.autoinit
import pycuda.driver as drv
import numpy as np
from pycuda.compiler import SourceModule

mod = SourceModule(open("2.cu").read())

upis_ispis = mod.get_function("upis_ispis")

a = np.ones((10,10), dtype=np.float32) * 5
b = np.ones_like(a) * 3
c = np.zeros_like(a)

upis_ispis(drv.In(a), drv.In(b), drv.Out(c), block=(10,10,1), grid=(1,1))
print(c)
