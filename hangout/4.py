# -*- coding: utf-8 -*-
'''

Simulirajmo ekonomiju. Inicijalizirajte GPUArray veličine 200 elemenata koji predstavlja financijsko stanje 200 jediniki u ekonomiji koju simuliramo. Incijalizirajte vrijednosti elemenata na 10. Definirajte dvije operacije

porast_pad_ekonomije(), koji svačije financijsko stanje množi sa koeficijentom k koji se određuje kao slučajan broj u intervalu [0.5, 1.5].
preraspodjela_trgovina(), koja vrši transakciju određenog iznosa (slučajan broj u rasponu [0, 0.5]) između dvije slučajno odabrane jedinke.
Izvedite Monte Carlo simulaciju veličine 100000 iteracija od kojih svaka iteracija vrši

slučajno između 5 i 10 operacija preraspodjela_trgovina
jednu operaciju porast_pad_ekonomije.

'''

import pycuda.autoinit
import pycuda.gpuarray as ga
import pycuda.curandom as rene
import numpy as np

def porast_pad_ekonomije(a):
    k = rene.rand(200, dtype=np.float32)+0.5
    a *= k

def preraspodjela_trgovina(a):
    i = np.random.randint(200)
    j = np.random.randint(200)
    while(i==j):
        j = np.random.randint(200)
    razlika = (0.5 - 0) * np.random.random_sample() + 0
    a_cpu = a.get()
    a_cpu[i] -= razlika
    a_cpu[j] += razlika
    a = ga.to_gpu(a_cpu)

print("Ekonomija, aaaaa daaaaaaaa, čoeče")

a = ga.zeros(200, dtype=np.float32)+10

print("Inicijalizacija (prije simulacije)")
print(a.get())


for i in range (1, 100):
    for j in range (1,np.random.randint(5,10)+1):
         preraspodjela_trgovina(a)
    porast_pad_ekonomije(a);


print("Nakon simulacije")
print(a.get())
