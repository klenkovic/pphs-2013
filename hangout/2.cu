// C kod
#include <math.h>

__global__ void upis_ispis(float *a, float *b, float *c){

  const int i = threadIdx.y * blockDim.x + threadIdx.x;

  float zbroj = 0;
  for(int k = 0; k<10; k++){
    zbroj += 2* pow(a[k+threadIdx.y*blockDim.x] , b[threadIdx.x+blockDim.x*k]);
  }

  c[i] = zbroj;

}
