import pycuda.autoinit
import pycuda.driver as drv
import pycuda.gpuarray as ga
import numpy as np
from pycuda.compiler import SourceModule

mod = SourceModule(open("redukcija_vise_blokova.cu").read())

find_min = mod.get_function("find_min")

a = np.random.rand(64).astype(np.float32)
mid_result_gpu = np.empty(4).astype(np.float32)
result_gpu = np.empty(1).astype(np.float32)

find_min(drv.Out(mid_result_gpu), drv.In(a), block=(16,1,1), grid=(4,1))

find_min(drv.Out(result_gpu), drv.In(mid_result_gpu), block=(4,1,1), grid=(1,1))

result_cpu = min(a)

print("PPočetno polje: ", a)
print("Rezultat prve redukcije GPU: ", mid_result_gpu)
print("Rezultat druge redukcije GPU: ", result_gpu)
print("Rezultat redukcije CPU: ", result_cpu)
