"""
 * Modificirajte prethodni zadatak da računa SKALARNI PRODUKT vektora umjesto norme.
"""
# NEDOVRSENO
import pycuda.autoinit
import pycuda.driver as drv
import pycuda.gpuarray as ga
import numpy as np
from pycuda.compiler import SourceModule

#sp = a1 * b1 + a2 * b2 + a3 * b3
#n_a = math.sqrt(a1 * a1 + a2 * a2 + a3 * a3)

mod = SourceModule(open("3.cu").read())

find_sp = mod.get_function("find_sp")

a =  2 * np.ones(64, dtype=np.float32)
b =  3 * np.ones(64, dtype=np.float32)
result_gpu = np.empty(1).astype(np.float32)


find_sp(drv.Out(mid_result_gpu), drv.In(a), drv.In(b), block=(16,1,1), grid=(1,1))
find_sp(drv.Out(result_gpu), drv.In(a), drv.In(b), block=(16,1,1), grid=(1,1))

result_cpu = sum(a * b)

print("Početno polje #1: ", a)
print("Početno polje #2: ", b)
print("Rezultat druge redukcije GPU: ", result_gpu)
print("Rezultat redukcije CPU: ", result_cpu)
