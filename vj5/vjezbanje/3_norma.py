"""
Python kod za normu vektora je oblika
"""
import pycuda.autoinit
import pycuda.driver as drv
import numpy as np
from pycuda.compiler import SourceModule

mod = SourceModule(open("3_norma.cu").read())

compute_norm = mod.get_function("compute_norm")

a = 2 * np.ones(16, dtype=np.float32)
result_gpu = np.empty(1).astype(np.float32)

compute_norm(drv.Out(result_gpu), drv.In(a), block=(16,1,1), grid=(1,1))

print(a)
print("GPU:", result_gpu)
