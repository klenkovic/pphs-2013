import pycuda.autoinit
import pycuda.driver as drv
import numpy as np
from pycuda.compiler import SourceModule

mod = SourceModule(open("4_skalarni.cu").read())

compute_scalar_product = mod.get_function("compute_scalar_product")

a = 2 * np.ones(16, dtype=np.float32)
b = 3 * np.ones(16, dtype=np.float32)
result_gpu = np.empty(1).astype(np.float32)

compute_scalar_product(drv.Out(result_gpu), drv.In(a), drv.In(b), block=(16,1,1), grid=(1,1))

result_cpu = sum(a * b)

print("GPU:", result_gpu)
print("CPU:", result_cpu)
