__global__ void compute_norm (float *dest, float *src)
{
  __shared__ float cache[16];

  const int idx = threadIdx.x;
  const int tidx = threadIdx.x;
  // kvadriramo brojeve kod spremanja u međuspremnik
  cache[tidx] = src[idx] * src[idx];
  int active = blockDim.x / 2;

  do
    {
      __syncthreads();
      if (tidx < active)
        {
          // redukcija korištenjem zbrajanja
          cache[tidx] += cache[tidx + active];
        }
      active /= 2;
    }
  while (active > 0);

  if (tidx == 0)
    {
      // korjenujemo kod spremanja u završni rezultat
      dest[blockIdx.x] = sqrt(cache[0]);
    }
}
