__global__ void compute_scalar_product (float *dest, float *src1, float *src2)
{
  __shared__ float cache[16];

  const int idx = threadIdx.x;
  const int tidx = threadIdx.x;
  // brojeve množimo kod spremanja u međuspremnik
  cache[tidx] = src1[idx] * src2[idx];
  int active = blockDim.x / 2;

  do
    {
      __syncthreads();
      if (tidx < active)
        {
          // redukcija korištenjem zbrajanja
          cache[tidx] += cache[tidx + active];
        }
      active /= 2;
    }
  while (active > 0);

  if (tidx == 0)
    {
      dest[blockIdx.x] = cache[0];
    }
}
