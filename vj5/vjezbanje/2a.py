"""
 * Modificirajte gornji kod da izračuna srednju 
 vrijednost umjesto da traži minimum.
 
 * Učinite da se kod izvodi na vektoru veličine 
 512 u 16 blokova. 
"""
import pycuda.autoinit
import pycuda.driver as drv
import numpy as np
from pycuda.compiler import SourceModule

mod = SourceModule(open("2a.cu").read())

find_avg = mod.get_function("find_avg")

a = np.ones(512, dtype=np.float32) #a = np.random.rand(64).astype(np.float32)
mid_result_gpu = np.empty(16).astype(np.float32)
result_gpu = np.empty(1).astype(np.float32)

find_avg(drv.Out(mid_result_gpu), drv.In(a), np.int32(32), block=(32,1,1), grid=(16,1))

find_avg(drv.Out(result_gpu), drv.In(mid_result_gpu), np.int32(16), block=(16,1,1), grid=(1,1))

result_cpu = sum(a)/len(a)

print(a)
print("mid_result_gpu:", mid_result_gpu)
print("CPU:", result_cpu)
print("GPU:", result_gpu)
