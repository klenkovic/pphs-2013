__global__ void find_min (float *dest, float *src)
{
  __shared__ float cache[16];

  const int idx = blockIdx.x * blockDim.x + threadIdx.x;
  const int tidx = threadIdx.x;
  cache[tidx] = src[idx];
  int active = blockDim.x / 2;

  do
    {
      __syncthreads();
      if (tidx < active)
        {
          cache[tidx] = fmin(cache[tidx], cache[tidx + active]);
        }
      active /= 2;
    }
  while (active > 0);

  if (tidx == 0)
    {
      dest[blockIdx.x] = cache[0];
    }
}
