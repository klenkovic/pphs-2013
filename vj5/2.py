import pycuda.autoinit
import pycuda.driver as drv
import pycuda.gpuarray as ga
import numpy as np
from pycuda.compiler import SourceModule

#sp = a1 * b1 + a2 * b2 + a3 * b3
#n_a = math.sqrt(a1 * a1 + a2 * a2 + a3 * a3)

mod = SourceModule(open("2.cu").read())

find_avg = mod.get_function("find_avg")

a =  2 * np.ones(16, dtype=np.float32)
result_gpu = np.empty(1).astype(np.float32)

find_avg(drv.Out(result_gpu), drv.In(a), np.int32(16), block=(16,1,1), grid=(1,1))

result_cpu = np.sqrt(sum(a * a))

print("PPočetno polje: ", a)
print("Rezultat druge redukcije GPU: ", result_gpu)
print("Rezultat redukcije CPU: ", result_cpu)
