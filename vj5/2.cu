__global__ void find_avg (float *dest, float *src, int n)
{
  __shared__ float cache[16];

  const int idx = blockIdx.x * blockDim.x + threadIdx.x;
  const int tidx = threadIdx.x;
  cache[tidx] = src[idx] * src[idx];
  int active = blockDim.x / 2;

  do
    {
      __syncthreads();
      if (tidx < active)
        {
          cache[tidx] += cache[tidx + active]; // ... sum(a * a)
        }
      active /= 2;
    }
  while (active > 0);

  if (tidx == 0)
    {
      dest[blockIdx.x] = sqrt(cache[0]); // ... math.sqrt(...)
    }
}
