"""
 * Promijenite konstantu 5 u testu u neku drugu konstantu. 
 Prolazi li test?

 * Definirajte funkciju koja prima broj i vraća kvadrat tog broja. 
 Definirajte test za nju s ulaznim podacima 3, 15, 24. 
"""
import modul1

def test_funkcija():
    assert modul1.funkcija() == 5
    print ("Test uspješno prolazi.")
def test_kvadrat(a):
    assert modul1.kvadrat(a) == a*a
    print("ok za", a)
def test2_kvadrat():
    assert modul1.kvadrat(3) == 9
    assert modul1.kvadrat(15) == 225
    assert modul1.kvadrat(24) == 576
    print("ok za 9, 15, 24")

test_funkcija()
test_kvadrat(3)
test_kvadrat(15)
test_kvadrat(24)
test2_kvadrat()
