"""
 * U datoteci modul1.py dodajte još jednu funkciju, nazovite ju 
moja_funkcija() s argumentima arg1 i arg2 koja vraća 42 * a * + 24 * arg2.

 * Stvorite datoteku modul2.py, u njoj definirajte
 funkciju say_hello() koja vraća niz znakova "I just came to say hello, o-o-o-o-o"

 * U datoteci program.py uključite drugi modul i pozovite obje funkcije. 
"""
import modul1
import modul2

if __name__ == "__main__":
    print(modul1.funkcija())
    print(modul1.varijabla)
    print(modul1.moja_funkcija(1, 2))
    print(modul2.say_hello())
