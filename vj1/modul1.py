import math

def f(a, b):
    """
    Funkcija f vraća zbroj brojeva a i b.

    Argumenti:
    a -- prvi br
    b -- drugi br

    Vraća:
    Zbroj a i b
    """
    return a + b

def funk2(a, b):
    return 4.2 * math.log(a) + math.cos(b)

def f3(a):
    print(a ** 2)

var = "PPHS"
