import modul2

def test_fun1(): 
    assert modul2.fun1(1, 2) == 1
    assert modul2.fun1(2, 1) == 2

def test_var():
    assert modul2.var == "LPR"
