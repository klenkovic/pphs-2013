def fun1(a, b):
    """
    Funckija fun1 vraća razultat operacije a ** b.

    Argumenti:
    a -- prvi br u izrazu
    b -- drugi br u izrazu

    Vraća:
    rezultat a ** b
    """
    return a ** b

def fun2(a, b):
    """
    Ispisuje niz znakova "LPR"
    """
    return (a + b) ** 2 

var = "LPR"
