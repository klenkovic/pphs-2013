import pycuda.autoinit
import pycuda.driver as drv
import numpy as np
from pycuda.compiler import SourceModule


mod = SourceModule(open("3.cu").read())

zbroj_vektora = mod.get_function("zbroj_vektora")

a = np.ones(400, dtype=np.float32)
b = np.ones(400, dtype=np.float32)

result_gpu = np.zeros_like(a)

zbroj_vektora (drv.Out(result_gpu), drv.In(a), drv.In(b), block=(400,1,1), grid=(1,1))

result_cpu = a + b

print result_cpu
