"""
 * Prilagodite kod primjera tako da računa zbroj oblika 2a+b.
 * Prilagodite kod primjera tako da računa zbroj oblika a+b+c. 
"""

import pycuda.autoinit
import pycuda.driver as drv
import numpy as np
from pycuda.compiler import SourceModule

mod = SourceModule(open("primjer_vektor.cu").read())

zbroj_vektora = mod.get_function("zbroj_vektora")

a = np.ones(400, dtype=np.float32)
b = np.ones(400, dtype=np.float32)
c = np.ones(400, dtype=np.float32)

result_gpu = np.zeros_like(a)

zbroj_vektora (drv.Out(result_gpu), drv.In(a), drv.In(b), drv.In(c), block=(400,1,1), grid=(1,1)) # zbroj_vektora (drv.Out(result_gpu), drv.In(a), drv.In(b), block=(400,1,1), grid=(1,1))
# N.B.: da bi imali gore block=(200,1,1), dobili bi rezultat -- s tim da bi prvih 200 vrijednosti u rezultatu bile tocne, a preostalih 200 bi bile popunjene s cim god je ostalo u gpu memoriji od prije


result_cpu = a + b + c # 2*a + b

print(result_gpu)
print(result_cpu)
