__global__ void zbroj_vektora1 (float *dest, float *a, float *b)
{
  const int i = threadIdx.x;
  dest[i] = 2 * a[i] + b[i];
}

__global__ void zbroj_vektora2 (float *dest, float *a, float *b, float *c)
{
  const int i = threadIdx.x;
  dest[i] = a[i] + b[i] + c[i];
}
