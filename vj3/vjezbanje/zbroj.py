"""
 * Prilagodite kod primjera tako da računa zbroj oblika 2a+b.
 * Prilagodite kod primjera tako da računa zbroj oblika a+b+c. 
 * Prilagodite kod prethodnog primjera tako da vektori imaju 
 500 elemenata (umjesto 400). 
"""
import pycuda.autoinit
import pycuda.driver as drv
import numpy as np
from pycuda.compiler import SourceModule

mod = SourceModule(open("zbroj.cu").read())

zbroj_vektora1 = mod.get_function("zbroj_vektora1")
zbroj_vektora2 = mod.get_function("zbroj_vektora2")

a = np.ones(400, dtype=np.float32)
b = np.zeros(400, dtype=np.float32)
c = np.zeros(400, dtype=np.float32)

result_gpu1 = np.zeros_like(a)
result_gpu2 = np.zeros_like(a)
#result_cpu = a + b

zbroj_vektora1(drv.Out(result_gpu1), drv.In(a), drv.In(b), block=(500,1,1), grid=(1,1))
zbroj_vektora2(drv.Out(result_gpu2), drv.In(a), drv.In(b), drv.In(c), block=(500,1,1), grid=(1,1))

#print("CPU rezultat\n", result_cpu)
print("GPU rezultat 1\n", result_gpu1)
print("GPU rezultat 2\n", result_gpu2)
#print("CPU i GPU daju isti rezultat?\n", result_cpu == result_gpu)
