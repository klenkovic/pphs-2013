"""
 * Provjerite možete li se s dva bloka izvesti zbrajanje 
 vektora od 3000 elemenata. (Čim je zadano ovako, 
 vjerojatno ne možete.)

 *  Izvedite zbrajanje vektora od 3000 elemenata u 3 bloka. 
"""
import pycuda.autoinit
import pycuda.driver as drv
import numpy as np
from pycuda.compiler import SourceModule

mod = SourceModule(open("zbroj2.cu").read())

zbroj_vektora1 = mod.get_function("zbroj_vektora1")

a = np.ones(400, dtype=np.float32)
b = np.zeros(400, dtype=np.float32)

result_gpu1 = np.zeros_like(a)
result_cpu = np.zeros_like(a)

result_cpu = a + b

zbroj_vektora1(drv.Out(result_gpu1), drv.In(a), drv.In(b), block=(1000,1,1), grid=(3,1))

print("GPU rezultat\n", result_gpu1)
print("CPU rezultat\n", result_cpu)
