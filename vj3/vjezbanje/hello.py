import pycuda.autoinit
import pycuda.driver as drv
import numpy as np
from pycuda.compiler import SourceModule

mod = SourceModule("""
#include <stdio.h>

__global__ void hello()
{
  printf ("Pozdrav s GPU-a!\\n");
}
""")

hello = mod.get_function("hello")

hello(block=(1,1,1), grid=(1,1))

print("Pozdrav s CPU-a!")
