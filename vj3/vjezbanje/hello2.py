"""
 * Unutar istog modula s izvornim kodom definirajte još 
 jednu funkciju i nazovite je my_hello() koja pozdravlja 
 na francuskom ("Salutations avec le GPU!"). Dohvatite je 
 u Pythonu i pozovite na isti način kao hello().

 * Unutar funkcije my_hello() inicijalizirajte dvije 
 varijable, var1 tipa int vrijednosti 480 i var2 tipa float 
 vrijednosti 2.075. Proučite dio koji se odnosi na C unutar 
 Wikipedijine stranice za printf i učinite da ispisuje i 
 Vrijednost varijable var1 je <vrijednost var1>, a vrijednost 
 varijable var2 <vrijednost var2>.

 * Varirajte brojeve u uređenoj trojci block i uređenom 
 paru grid za vašu funkciju, stavite primjerice 2 ili 3 
 umjesto 1 na nekim mjestima. Što uočavate? 
"""
import pycuda.autoinit
import pycuda.driver as drv
import numpy as np
from pycuda.compiler import SourceModule

mod = SourceModule(open("hello2.cu").read())

hello = mod.get_function("hello")
my_hello = mod.get_function("my_hello")

hello(block=(1,1,1), grid=(1,1))
my_hello(block=(2,3,1), grid=(2,1))

print("Pozdrav s CPU-a!")
