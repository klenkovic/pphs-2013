#include <stdio.h>

__global__ void hello()
{
  printf ("Pozdrav s GPU-a!\n");
}

__global__ void my_hello()
{
  int var1 = 480;
  float var2 = 2.075;
  printf ("Salutations avec le GPU!\n");
  printf ("Vrijednost varijable var1 je %d, a vrijednost varijable var2 %f.\n", var1, var2);
}
