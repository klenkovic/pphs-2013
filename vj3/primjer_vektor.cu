__global__ void zbroj_vektora (float *dest, float *a, float *b, float *c) // __global__ void zbroj_vektora (float *dest, float *a, float *b)
{
  const int i = threadIdx.x;
  dest[i] = a[i] + b[i] + c[i]; // dest[i] = 2*a[i] + b[i];
}
