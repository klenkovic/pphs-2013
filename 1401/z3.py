
'''                                                                                                                                          
Inicirajte GPUArray sa 10 redaka i 5 stupaca u kojem cemo retke tretirati kao vektore. Napisite kod koji vrsi zbrajanje svakog vektora sa svakim vektorom a rezulta vraca koa GPUArray velicine (10 povrh 2) redaka i 5 stupaca.

'''

import pycuda.autoinit
import pycuda.driver as drv
import pycuda.cumath
import pycuda.curandom
import numpy as np
import pycuda.gpuarray as ga
from pycuda.compiler import SourceModule

vektori = ga.GPUArray((10,5), dtype=np.float32)
zbroj = ga.GPUArray((45,5), dtype=np.float32)

#k = 0
for i in range(10):
    for j in range(i+1, 10):
        zbroj = vektori[i] + vektori[j]
        #k += 1
        print(zbroj)
