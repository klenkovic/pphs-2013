
'''                                                                                                                                          
Napisite program koji vrsi jednoblokovno modificirano mnozenje matrica oblika
\sum_k 2 * pow(a_{ik}, b_{kj})
(Mnozenje matrica je \sum_k a_{ik} * b_{ik})

'''

import pycuda.autoinit
import pycuda.driver as drv
import numpy as np
from pycuda.compiler import SourceModule

mod = SourceModule(open("z2.cu").read())

prod = mod.get_function("mod_prod")

a = np.ones((10,10), dtype=np.float32)
b = np.ones((10,10), dtype=np.float32)

c = np.empty_like(a)

prod(drv.Out(c), drv.In(a), drv.In(b), block=(10,10,1), grid=(1,1))

print(c)
