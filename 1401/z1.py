
'''                                                                                                                                          
Napisite program koji vrsi jednoblokovno modificirano zbrajanje matrica formata 10*10:                                                               
2 * sin(a) + 3 * cos(b) + 4                                                                                                                  
Pri cemu tu operaciju ucinite device funkcijom.                                                                                              
'''

import pycuda.autoinit
import pycuda.driver as drv
import numpy as np
from pycuda.compiler import SourceModule

mod = SourceModule(open("z1.cu").read())

zbrajanje = mod.get_function("zbrajanje")

a = np.ones((10,10), dtype=np.float32)
b = np.ones((10,10), dtype=np.float32)

c = np.empty_like(a)

zbrajanje(drv.Out(c), drv.In(a), drv.In(b), block=(10,10,1), grid=(1,1))

print(c)
