/*
Napisite program koji vrsi jednoblokovno modificirano mnozenje matrica oblika
\sum_k 2 * pow(a_{ik}, b_{kj})
(Mnozenje matrica je \sum_k a_{ik} * b_{ik})
*/

#include <math.h>

__global__ void mod_prod(float * c, float * a, float * b){

  int i = threadIdx.y * blockDim.x + threadIdx.x;
  double zbroj = 0;
  for (int k=0;k<10;k++){
    
    
    zbroj += 2 * pow(a[k * blockDim.x + threadIdx.x], b[threadIdx.y * blockDim.x + k]);


  }
  c[i] = zbroj;

}
