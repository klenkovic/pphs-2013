/*
Napisite program koji vrsi jednoblokovno modificirano zbrajanje matrica formata 10*10:
2 * sin(a) + 3 * cos(b) + 4
Pri cemu tu operaciju ucinite device funkcijom.
*/

#include <math.h>

__device__ float zbroj(float a, float b){
  return 2 * sin(a) + 3 * cos(b) + 4;
}

__global__ void zbrajanje(float * c, float * a, float * b){

  int i = threadIdx.y * blockDim.x + threadIdx.x;

  c[i] = zbroj(a[i],b[i]);

}
