"""
 * element na i u odredišnoj matrici postavlja vrijednost: indeks bloka po x-u 
   pomnožen sa indeksom bloka po y-u zbrojen sa elementom prve matrice i zbrojen 
   sa elementom druge matrice
"""
import pycuda.autoinit
import pycuda.driver as drv
import pycuda.gpuarray as ga
import numpy as np
from pycuda.compiler import SourceModule

n = 100

mod = SourceModule(open("4.cu").read())

zbroji_matrice = mod.get_function("zbroji_matrice")

a = np.ones((n, n), dtype=np.float32) 
b = np.ones((n, n), dtype=np.float32)

result_gpu = np.empty_like(a)

zbroji_matrice(drv.Out(result_gpu), drv.In(a), drv.In(b), np.int32(n), block=(10,10,10), grid=(10,10))

result_cpu = a + b

print("CPU: ", result_cpu)
print("GPU: ", result_gpu)
