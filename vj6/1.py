import pycuda.autoinit
import pycuda.driver as drv
import pycuda.gpuarray as ga
import numpy as np
from pycuda.compiler import SourceModule

mod = SourceModule(open("1.cu").read())

zbroji_matrice = mod.get_function("zbroji_matrice")

a = np.ones((20, 20), dtype=np.float32) 
b = np.ones((20, 20), dtype=np.float32)

result_gpu = np.empty_like(a)

zbroji_matrice(drv.Out(result_gpu), drv.In(a), drv.In(b), block=(20,20,1), grid=(1,1))

result_cpu = a + b

print("CPU: ", result_cpu)
print("GPU: ", result_gpu)
