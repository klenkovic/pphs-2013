__global__ void zbroji_matrice (float *dest, float *a, float *b)
{
  const int i = threadIdx.y * blockDim.x + threadIdx.x;
  dest[i] = a[i] + b[i];
}

/*
(0, 0), (0, 1), (0, 2)
1 5 3
4 2 8
(1, 0), (1, 1), (1, 2)

1 0
5 1
3 2
4 3
2 4
8 5
 */
