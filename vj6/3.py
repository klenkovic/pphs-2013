"""
 * Prilagodite gornji kod tako da se zrnu prosljeđuje i veličina 
   matrice; za to trebate napraviti tri stvari:
 * U zrnu promijeniti kod da potpis bude __global__ void zbroji_matrice 
   (float *dest, float *a, float *b, int n),
 * Inicijalizirati u Python kodu n na željenu vrijednost odgovarajućeg numpy 
   tipa (proučite ranije navedeni popis),
 * Izvesti poziv funkcije s matrix_sum(result_gpu, a_gpu, b_gpu, n, ...), 
   obzirom da se kod prosljeđivanja argumenata po vrijednosti može raditi 
   s nekim numpy tipovima (za detalje proučite dokumentaciju objekta pycuda.driver.Function).
"""
import pycuda.autoinit
import pycuda.driver as drv
import pycuda.gpuarray as ga
import numpy as np
from pycuda.compiler import SourceModule

n = 100

mod = SourceModule(open("3.cu").read())

zbroji_matrice = mod.get_function("zbroji_matrice")

a = np.ones((n, n), dtype=np.float32) 
b = np.ones((n, n), dtype=np.float32)

result_gpu = np.empty_like(a)

zbroji_matrice(drv.Out(result_gpu), drv.In(a), drv.In(b), np.int32(n), block=(10,10,10), grid=(10,10))

result_cpu = a + b

print("CPU: ", result_cpu)
print("GPU: ", result_gpu)
