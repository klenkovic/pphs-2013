"""
 * Prilagodite kod tako da množi matrice formata (200, 200)
 u 10 blokova po x koordinati i 10 blokova po y koordinati. 
"""
import pycuda.autoinit
import pycuda.driver as drv
import numpy as np
from pycuda.compiler import SourceModule
#np.set_printoptions(threshold=np.nan) # print cijela matrica

mod = SourceModule(open("3_test.cu").read())

matrix_mult = mod.get_function("matrix_mult")

a = np.ones((6, 6), dtype=np.float32)
b = np.ones((6, 6), dtype=np.float32)

result_gpu = np.empty_like(a)

matrix_mult(drv.Out(result_gpu), drv.In(a), drv.In(b), block=(3,3,1), grid=(2,2))

# result_cpu = np.dot(a, b)
result_cpu = np.matrix(a) * np.matrix(b)

print("CPU:", result_cpu)
print("GPU:", result_gpu)
