import pycuda.autoinit
import pycuda.driver as drv
import numpy as np
from pycuda.compiler import SourceModule

mod = SourceModule(open("1.cu").read())
matrix_sum = mod.get_function("matrix_sum")

n = np.int32(20)
a = np.ones((n, n), dtype=np.float32)
b = np.ones((n, n), dtype=np.float32)

result_gpu = np.empty_like(a)

matrix_sum(drv.Out(result_gpu), drv.In(a), drv.In(b), n, block=(20,20,1), grid=(1,1))

result_cpu = a + b

print("CPU rezultat\n", result_cpu)
print("GPU rezultat\n", result_gpu)
