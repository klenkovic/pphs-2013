//#include <stdio.h>

__global__ void mat_vec_mult (float *dest, float *mat, float *vec)
{
  const int idx = blockDim.x * threadIdx.y + threadIdx.x;
  //printf ("%d\n", gridDim.x);
  float sum_product = 0;
  for (int i = 0; i < 16; i++)
    {
      sum_product += mat[idx * 20 + i] * vec[i];
    }

  dest[idx] = sum_product;
}
