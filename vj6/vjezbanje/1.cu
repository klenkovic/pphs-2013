__global__ void matrix_sum (float *dest, float *a, float *b, int n)
{
  const int i = blockDim.x * threadIdx.y + threadIdx.x;
  dest[i] = a[i] + b[i];
}
