"""
 * Promijenite veličinu matrice na (16, 16), a veličinu 
 vektora na 16.

 * Promijenite kod tako da se izvodi u 16 niti po x-u i 
 16 niti po y-u, i da se pritom redukcija (zbrajanje produkata) 
 izvodi paralelno na način koji smo već ranije opisali.
"""
import pycuda.autoinit
import pycuda.driver as drv
import numpy as np
from pycuda.compiler import SourceModule

mod = SourceModule(open("2.cu").read())

mat_vec_mult = mod.get_function("mat_vec_mult")

a = np.ones((16, 16), dtype=np.float32)
b = np.ones(16, dtype=np.float32)

#result_gpu = np.empty_like(16, dtype=np.float32)
result_gpu = np.empty(1).astype(np.float32)

mat_vec_mult(drv.Out(result_gpu), drv.In(a), drv.In(b), block=(16,16,1), grid=(1,1))

result_cpu = np.dot(a, b)

print("CPU rezultat\n", result_cpu)
print("GPU rezultat\n", result_gpu)
