__global__ void matrix_mult (float *result, float *mat1, float *mat2)
{
  const int idx = gridDim.x * blockIdx.y * blockDim.x * blockDim.y + blockDim.x * blockDim.y * blockIdx.x + threadIdx.y * blockDim.x + threadIdx.x;

  float sum_product = 0;
  for (int k = 0; k < 6; k++)
    {
      sum_product += mat1[threadIdx.y * blockDim.x + k] * mat2[k * blockDim.x + threadIdx.x];
    }

  result[idx] = sum_product;
}
