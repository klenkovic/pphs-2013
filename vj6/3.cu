__global__ void zbroji_matrice (float *dest, float *a, float *b, int n)
{
  //const int i = (blockIdx.y * blockDim.y + threadIdx.y) * blockDim.x + blockIdx.x * blockDim.x  + threadIdx.x;
  const int i = blockIdx.y * gridDim.x * blockDim.y * blockDim.x + threadIdx.y * blockDim.x + blockIdx.x * blockDim.y * blockDim.x + threadIdx.x;
  dest[i] = n * (a[i] + b[i]); // samo da se iskoristi ovaj n
}

/*
(0, 0), (0, 1), (0, 2)
1 5 3
4 2 8
(1, 0), (1, 1), (1, 2)

a * broj_stupaca + b

1 0
5 1
3 2
4 3
2 4
8 5
 */
