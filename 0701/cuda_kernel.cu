/*

Deklarirajte varijablu pi tipa float u konstantnoj memoriji i postavite ju na vrijednost 3.14159.

Definirajte zrno pi_zapisi(float *polje). Unutar modula, ali van zrna, definirajte polje pi_polje tipa float veličine 200 elemenata u globalnoj memoriji, u koje će svaka od niti zrna pi_zapisi() upisati vrijednost konstante pi. Pokrenite izvođenje zrna sa 20 niti po bloku i 10 blokova. (Uputa: za provjeru točnosti koda možete iskoristiti printf().) 

-----

def. device f-ju puta_dva_plus_jedan koja u polju tipa float element sa indeksom i mnozi sa 2 i pribraja mu 1
pozovite je nakon f-je zapisi_pi nad istim poljem sa indeksom
 */
#include <stdio.h>

__constant__ float pi = 3.14159;

__device__ float pi_polje[200];

__device__ void zapisi_pi(float * p, int i){
  p[i] = pi;
}

__device__ void puta_dva_plus_jedan(float * p, int i){
  p[i] = p[i] * 2 + 1;
}

__global__ void pi_zapisi(float *drugopolje){
  
  //printf("%1.12f\n", pi);
  int i = blockIdx.x * blockDim.x + threadIdx.x;
  zapisi_pi(pi_polje, i);
  puta_dva_plus_jedan(pi_polje, i);
  //pi_polje[i] = pi;
  drugopolje[i] = pi_polje[i];
}
