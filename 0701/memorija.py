import pycuda.autoinit
import pycuda.driver as drv
import pycuda.gpuarray as ga
import numpy as np
from pycuda.compiler import SourceModule

mod = SourceModule(open("cuda_kernel.cu").read())

pi_zapisi  = mod.get_function("pi_zapisi")

polje = np.empty(200, dtype=np.float32)
pi_zapisi(drv.Out(polje),block=(20,1,1),grid=(10,1))

print(polje)
