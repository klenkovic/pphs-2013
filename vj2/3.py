"""
 * Stvorite dva dvodimenzionalna polja a i b oblika (3, 3) s proizvoljnim 
vrijednostima, i to tako da prvo ima elemente tipa numpy.float32, a 
drugo elemente tipa numpy.float64.
 * Izračunajte 2 * a + b, cos(a), sqrt(b). Uočite kojeg su tipa polja 
koja dobivate kao rezultate.
 * Množenje matrica izvodite funkcijom numpy.dot(); proučite njenu 
dokumentaciju i izvedite ju na svojim poljima.
"""
import numpy as np

a  = np.array([[1, 2, 3],
               [4, 5, 6],
               [7, 8, 9]], dtype=np.float32)

b = np.array([[10, 20, 30],
               [40, 50, 60],
               [70, 80, 90]], dtype=np.float64)

print(2 * a + b, (2 * a + b).dtype)
print(np.cos(a))
print(np.sqrt(b))
print(np.dot(a, b))
