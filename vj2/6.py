"""
 * Stvorite dvije datoteke, nazovite ih matrica_a.txt i matrica_b.txt. 
Matrica u prvoj datoteci neka bude oblika (3, 5), a u drugoj datoteci 
oblika (5, 4).
 * Izvršite čitanje podataka, a zatim izračunajte produkt dvaju matrica. 
Možete li izračunati oba produkta ili samo jedan? Objasnite zašto.
"""
import numpy as np

pod_a = open("matrica_a.txt")
a = np.loadtxt(pod_a)

pod_b = open("matrica_b.txt")
b = np.loadtxt(pod_b)

print(np.dot(a, b)) # popravit
