import numpy as np

a = np.array([[1, 2, 3, 4]])
b = np.array([[5, 6, 7, 8]])
# operacije koje rade element po element
c = a + b
d = b - a
e = a ** 2
f = a * b
g = np.sin(a)
h = np.exp(b)
# operacije koje mijenjaju pocetni array
a += b
b *= 3

print a,b,c,d,e,f,g,h
