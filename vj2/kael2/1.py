import numpy as np

a = np.array([[9,13,5],[1,11,7],[3,7,2],[6,0,7]])

print a

print a.dtype
print a.size
print a.shape
print a.ndim
print a.itemsize

b = np.array([[9,13,5],[1,11,7],[3,7,2],[6,0,7]], dtype=np.float32)
print b

c = np.zeros((5,5),dtype=np.float32)
print c

d = np.ones((1000,1000), dtype=np.int32)
print d

# e = [[1 for x in range(1000)] for x in range(10000)]
# print e
