import numpy as np

a = np.array([[1, 2, 3], [4, 5, 6], [7, 8, 9]], dtype=np.float32)
b = np.array([[10, 20, 30], [40, 50, 60], [70, 80, 90]], dtype=np.float64)

print a
print b

print 2*a+b
print np.cos(a)
print np.sqrt(b)

print np.dot(a,b)
