"""
  * Stvorite dvije datoteke, nazovite ih matrica_a.txt
  i matrica_b.txt. Matrica u prvoj datoteci neka bude oblika 
  (3, 5), a u drugoj datoteci oblika (5, 4).

  * Izvršite čitanje podataka, a zatim izračunajte produkt 
  dvaju matrica. Možete li izračunati oba produkta ili samo 
  jedan? Objasnite zašto. 
"""
import numpy as np

p1 = open("mat_a.txt")
a = np.loadtxt(p1)
p2 = open("mat_b.txt")
b = np.loadtxt(p2)
p1.close()
p2.close()

print(a)
print(b)

c = np.dot(a, b)
print(c)
