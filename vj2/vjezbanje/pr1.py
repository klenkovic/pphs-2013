import numpy as np

a = np.array([[1, 2, 3, 4]])
b = np.array([[5, 6, 7, 8]])
print(a)
print(b)

print("operacije koje rade element po element")
print("a + b =", a + b)
print("b - a =", b - a)
print("a ** 2 =", a ** 2)
print("a * b =", a * b)
print("np.sin(a) =", np.sin(a))
print("np.exp(b) =", np.exp(b))

print("operacije koje mijenjaju početni array")
a += b
print("a += b -->", a)
b *= 3
print("b *= 3 -->", b)
