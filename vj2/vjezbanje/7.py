"""
 * Stvorite višedimenzionalno polje a proizvoljnih 
 cijelobrojnih vrijednosti oblika (5,4). Učinite iduće:

   * Trajno promijenite oblik polja u (2,10).
   * Transponirajte polje uz povećavanje svih 
   vrijednosti polja za kosinus od 5.
   * stvorite jednodimenzionalno polje b preoblikovanjem 
   polja a tako da su vrijednosti elementa u b dvostruko 
   veće od vrijednosti elemenata iz a 
"""
import numpy as np

a = np.array([[1, 2, 3, 4],
              [5, 6, 7, 8],
              [9, 10, 11, 12],
              [13, 15, 16, 17],
              [18, 19, 20, 21]], dtype=np.float32)

a = np.reshape(a, (2, 10))
print(a)

a += np.cos(5)
print(a)

b = np.ravel(a) 
b *= 2
print(b)
