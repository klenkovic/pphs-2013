"""
 * Stvorite polje s vrijednostima
9 13 5
1 11 7
3 7 2
6 0 7
 * Saznajte mu oblik, duljinu i tip elemenata i veličinu elementa u bajtovima.
 * Stvorite polje s istim vrijednostima, ali tako da su elementi tipa float.
"""
import numpy as np

a = np.array([[9, 13, 5], 
              [1, 11, 7], 
              [3, 7, 2], 
              [6, 0, 7]])

print("a =", a)
print("oblik:", a.shape, ", duljina:", a.size, ", tip:", a.dtype, ", veličina u bajtovima:", a.itemsize)

b = np.float64(a) # ili u prvoj matrici dodat jednom broju točku 

print("b =", b)
