"""
 * Stvorite polje u kojem su sve vrijednosti jednake 9.45 tipa float64 
i pretvorite ga u polje tipa float32 i rezultat spremite u novo polje. 
Uočavate li gubitak preciznosti?
 * Pretvorite dobiveno polje tipa float32 u polje tipa float64. 
Je li rezultat jednak početnom polju?
 * Iskoristite round da na rezultirajućem polju tipa float64 dobijete 
iste vrijednosti kao na početnom.
Napomena: rezultat ovog zadatka uvelike ovisi o računalu na kojem radite.
"""
import numpy as np

a = 9.45 * np.ones((3, 3), dtype=np.float64)
print(a)

b = a.astype(np.float32)
print(b)

b.astype(np.float64)
print(b)

b = np.round(b.astype(np.float64), 2)
print("round:", b)
