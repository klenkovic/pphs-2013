"""
 * Stvorite polje nula oblika (5, 5) u kojem su elementi tipa numpy.float32.
 * Stvorite polje jedinica oblika (1000, 1000). Pokušajte ga ispisati 
naredbom print. Što se dogodi?
"""
import numpy as np

nule = np.zeros((5, 5), dtype=np.float32)
print(nule)

jed = np.ones((1000, 1000)) # ((10 ** 3, 10 ** 3))
print(jed)

#mat = [[1 for x in range(10000)] for x in range(10000)] # matrica 10000 * 10000
#print(mat)
